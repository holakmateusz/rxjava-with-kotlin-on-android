package com.example.rxjavacounter

import android.app.Application
import com.example.rxjavacounter.di.appModule
import org.koin.core.context.startKoin

class StartApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(appModule)
        }
    }
}