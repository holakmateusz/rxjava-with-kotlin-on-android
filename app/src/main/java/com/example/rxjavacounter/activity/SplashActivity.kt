package com.example.rxjavacounter.activity

import android.content.Intent
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.rxjavacounter.R
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity(R.layout.activity_splash) {

    private val splashTime by lazy { TimeUnit.SECONDS.toMillis(3) }
    private val mainActivityTrigger by lazy { Handler() }

    override fun onResume() {
        super.onResume()
        mainActivityTrigger.postDelayed({ goToMainActivity() }, splashTime)
    }

    override fun onPause() {
        super.onPause()
        mainActivityTrigger.removeCallbacksAndMessages(null)
    }

    private fun goToMainActivity() {
        val mainActivityIntent = Intent(this, MainActivity::class.java)
        startActivity(mainActivityIntent)
        finish()
    }
}