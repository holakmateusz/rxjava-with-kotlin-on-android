package com.example.rxjavacounter.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MainViewModel : ViewModel() {
    private val counter = MutableLiveData<Long>()
    private val compositeDisposable by lazy { CompositeDisposable() }

    init {
        onViewCreated()
    }

    internal fun getCounter(): LiveData<Long> {
        return counter
    }

    private fun onViewCreated() {
        val disposable = Observable.intervalRange(0, 60, 3, 1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                counter.value = it
            }
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}